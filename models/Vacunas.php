<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacunas".
 *
 * @property int $identificador
 * @property string|null $nombre
 * @property int|null $n_dosis
 * @property string|null $enfermedad_trata
 *
 * @property VacunasAnimal $vacunasAnimal
 * @property Mascotas[] $refAnimals
 */
class Vacunas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacunas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_dosis'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['enfermedad_trata'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'identificador' => 'Identificador',
            'nombre' => 'Nombre',
            'n_dosis' => 'N Dosis',
            'enfermedad_trata' => 'Enfermedad Trata',
        ];
    }

    /**
     * Gets query for [[VacunasAnimal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVacunasAnimal()
    {
        return $this->hasOne(VacunasAnimal::className(), ['ref_animal' => 'identificador']);
    }

    /**
     * Gets query for [[RefAnimals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRefAnimals()
    {
        return $this->hasMany(Mascotas::className(), ['identificador' => 'ref_animal'])->viaTable('vacunas_animal', ['ref_animal' => 'identificador']);
    }
}
