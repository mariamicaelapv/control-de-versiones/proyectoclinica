<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "referencia".
 *
 * @property int|null $ref_vacuna_animal
 * @property int $ref_animal
 *
 * @property VacunasAnimal $refAnimal
 */
class Referencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'referencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_vacuna_animal', 'ref_animal'], 'integer'],
            [['ref_animal'], 'required'],
            [['ref_animal'], 'unique'],
            [['ref_animal'], 'exist', 'skipOnError' => true, 'targetClass' => VacunasAnimal::className(), 'targetAttribute' => ['ref_animal' => 'ref_vacuna']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ref_vacuna_animal' => 'Ref Vacuna Animal',
            'ref_animal' => 'Ref Animal',
        ];
    }

    /**
     * Gets query for [[RefAnimal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRefAnimal()
    {
        return $this->hasOne(VacunasAnimal::className(), ['ref_vacuna' => 'ref_animal']);
    }
}
