<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacunas_animal".
 *
 * @property int $ref_vacuna
 * @property int|null $ref_cita
 *
 * @property Referencia $referencia
 * @property Vacunas $refVacuna
 */
class VacunasAnimal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacunas_animal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_vacuna'], 'required'],
            [['ref_vacuna', 'ref_cita'], 'integer'],
            [['ref_vacuna'], 'unique'],
            [['ref_vacuna'], 'exist', 'skipOnError' => true, 'targetClass' => Vacunas::className(), 'targetAttribute' => ['ref_vacuna' => 'identificador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ref_vacuna' => 'Ref Vacuna',
            'ref_cita' => 'Ref Cita',
        ];
    }

    /**
     * Gets query for [[Referencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReferencia()
    {
        return $this->hasOne(Referencia::className(), ['ref_animal' => 'ref_vacuna']);
    }

    /**
     * Gets query for [[RefVacuna]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRefVacuna()
    {
        return $this->hasOne(Vacunas::className(), ['identificador' => 'ref_vacuna']);
    }
}
