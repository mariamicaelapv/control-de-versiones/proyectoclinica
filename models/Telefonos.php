<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $ref_vet
 * @property string|null $telefono
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_vet'], 'required'],
            [['ref_vet'], 'integer'],
            [['telefono'], 'string', 'max' => 20],
            [['ref_vet'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ref_vet' => 'Ref Vet',
            'telefono' => 'Telefono',
        ];
    }
}
