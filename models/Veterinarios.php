<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "veterinarios".
 *
 * @property int $Identificador
 * @property string|null $DNI
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $hora_entrada
 * @property string|null $hora_salida
 */
class Veterinarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hora_entrada', 'hora_salida'], 'safe'],
            [['DNI'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 15],
            [['apellidos'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Identificador' => 'Identificador',
            'DNI' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'hora_entrada' => 'Hora Entrada',
            'hora_salida' => 'Hora Salida',
        ];
    }
}
