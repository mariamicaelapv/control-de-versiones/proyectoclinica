<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $identificador
 * @property string|null $telefono
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono', 'apellidos'], 'string', 'max' => 20],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'identificador' => 'Identificador',
            'telefono' => 'Telefono',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }
}
