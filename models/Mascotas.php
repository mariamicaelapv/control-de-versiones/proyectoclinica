<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mascotas".
 *
 * @property int $identificador
 * @property int|null $ref_cliente
 * @property string|null $nombre
 * @property string|null $sexo
 * @property string|null $raza
 */
$model['ref_mascota'] = 'identificador';
class Mascotas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mascotas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['nombre'], 'string', 'max' => 15],
            [['sexo'], 'string', 'max' => 10],
            [['raza'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'identificador' => 'Identificador',
            'ref_cliente' => 'Ref Cliente',
            'nombre' => 'Nombre',
            'sexo' => 'Sexo',
            'raza' => 'Raza',
        ];
    }
  
}
