<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citas".
 *
 * @property int $identificador
 * @property int|null $ref_mascota
 * @property string|null $fecha_hora
 * @property string|null $Observaciones
 *
 * @property Mascotas $refMascota
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_mascota'], 'integer'],
            [['fecha_hora'], 'safe'],
            [['Observaciones'], 'string', 'max' => 500],
            [['ref_mascota'], 'exist', 'skipOnError' => true, 'targetClass' => Mascotas::className(), 'targetAttribute' => ['ref_mascota' => 'identificador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'identificador' => 'Identificador',
            'ref_mascota' => 'Ref Mascota',
            'fecha_hora' => 'Fecha Hora',
            'Observaciones' => 'Observaciones',
        ];
    }

    /**
     * Gets query for [[RefMascota]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRefMascota()
    {
        return $this->hasOne(Mascotas::className(), ['identificador' => 'ref_mascota']);
    }
}
