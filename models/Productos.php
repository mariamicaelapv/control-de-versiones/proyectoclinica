<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $identificador
 * @property string|null $nombre
 * @property string|null $tipo_producto
 * @property int|null $precio
 * @property string|null $marca
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'integer'],
            [['nombre'], 'string', 'max' => 15],
            [['tipo_producto'], 'string', 'max' => 30],
            [['marca'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'identificador' => 'Identificador',
            'nombre' => 'Nombre',
            'tipo_producto' => 'Tipo Producto',
            'precio' => 'Precio',
            'marca' => 'Marca',
        ];
    }
}
