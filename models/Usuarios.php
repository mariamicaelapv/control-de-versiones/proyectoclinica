<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property int $activate
 * @property string $verification_code
 * @property int $role
 * @property string|null $telefono
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property Mascotas[] $mascotas
 * @property Veterinarios[] $veterinarios
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password', 'authKey', 'accessToken', 'verification_code','nombre','apellidos','dni','telefono'], 'required'],
            [['activate', 'role'], 'integer'],
            [['username'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 80],
            [['password', 'authKey', 'accessToken', 'verification_code'], 'string', 'max' => 250],
            [['telefono', 'apellidos'], 'string', 'max' => 20],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'activate' => 'Activate',
            'verification_code' => 'Verification Code',
            'role' => 'Role',
            'telefono' => 'Telefono',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[Mascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMascotas()
    {
        return $this->hasMany(Mascotas::className(), ['ref_cliente' => 'id']);
    }

    /**
     * Gets query for [[Veterinarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinarios()
    {
        return $this->hasMany(Veterinarios::className(), ['ref_usuario' => 'id']);
    }
}
