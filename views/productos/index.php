<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
    <body class="is-preload">
        <div id="page-wrapper">
            <header id="header">
                 <h1><a href="http://localhost/Clinica/web/site/user">Clinica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/user">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <header>
                    <h2>Productos</h2>
                    <p>Productos disponibles para su compra en tienda.</p>
                 </header>
            </section>
            <section id="main" class="container medium">
                <div class="box">

<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

 


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          
            'nombre',
            'tipo_producto',
            'precio',
            'marca',

           
        ],
    ]); ?>


</div>
                </div>
            </section>
        </div>
    </body>
</html>