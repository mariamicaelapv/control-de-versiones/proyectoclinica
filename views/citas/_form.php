<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */
/* @var $form yii\widgets\ActiveForm */
?>
<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
              <header id="header">
                 <h1><a href="http://localhost/Clinica/web/site/user">Clinica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/user">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <header>
                    <h2>Solicitar cita</h2>
                    <p>Rellene los campos</p>
                 </header>
            </section>
            <section id="main" class="container medium">
                <div class="box">

<div class="citas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $listamascotas=ArrayHelper::map($model_mascotas, 'identificador','nombre');?>
    
    <?=$form->field($model, 'identificador')->dropDownList($listamascotas)->label('Nombre Mascota')?>

   <?php echo $form->field($model,'fecha_hora')->
    widget(DateTimePicker::className(),[
  'language' => 'es',
  'template' => '{input}',
        'size' => 'ms',
    'pickButtonIcon' => 'glyphicon glyphicon-time',
    'clientOptions' => [
       
       
        'autoclose' => true,
        'linkFormat' => 'HH:ii P', // if inline = true
        // 'format' => 'HH:ii P', // if inline = false
        'todayBtn' => true
    ]
]); ?>


    <?= $form->field($model, 'Observaciones')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
                </div>
            </section>
        </div>
    </body>
</html>