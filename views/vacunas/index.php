<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<html>
    <head>
        <title>Clínica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
        <header id="header">
                 <h1><a href="http://localhost/Clinica/web/site/admin">Clínica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/admin">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <h2>Creación de vacunas</h2>
                <p>Vacunas creadas en la base de datos</p>
               </section>
            <section id="main" class="container medium">
                <div class="box">
                    
                    
               
<div class="vacunas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vacunas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'identificador',
            'nombre',
            'n_dosis',
            'enfermedad_trata',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
 </div>
            </section>
</html>
