<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vacunas */


?>

<html>
    <head>
        <title>Clínica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
        <header id="header">
                 <h1><a href="http://localhost/Clinica/web/site/admin">Clínica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/admin">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <h2>Crear</h2>
                <p>Poner toda la informacion requerida para crear las vacunas.</p>
               </section>
            <section id="main" class="container medium">
                <div class="box">

    
<div class="vacunas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
                </div>
            </section>
</html>