<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\VacunasAnimal */
/* @var $form yii\widgets\ActiveForm */
?>
<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
              <header id="header">
                 <h1><a href="http://localhost/Clinica/web/site/admin">Clinica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/admin">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <header>
                    <h2>Vacunas puestas</h2>
                    <p>Rellene los campos sobre la vacuna puesta al animal</p>
                 </header>
            </section>
            <section id="main" class="container medium">
                <div class="box">
<div class="vacunas-animal-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php $listavacuna=ArrayHelper::map($model_vacunas,'ref_cita','nombre');?>
     <?=$form->field($model, 'ref_cita')->dropDownList($listavacuna)->label('Nombre Vacuna')?>

     <?php $listamascotas=ArrayHelper::map($model_mascotas, 'ref_cita','nombre');?>
    
    <?=$form->field($model, 'ref_cita')->dropDownList($listamascotas)->label('Nombre Mascota')?>
  

      <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
                </div>
            </section>
</html>
