<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacunasAnimal */

$this->title = 'Update Vacunas Animal: ' . $model->ref_vacuna;
$this->params['breadcrumbs'][] = ['label' => 'Vacunas Animals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ref_vacuna, 'url' => ['view', 'id' => $model->ref_vacuna]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacunas-animal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_vacunas'=>$model_vacunas,
    ]) ?>

</div>
