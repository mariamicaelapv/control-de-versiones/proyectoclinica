<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VacunasAnimal */


?>
<div class="vacunas-animal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_vacunas'=>$model_vacunas,
        'model_mascotas'=>$model_mascotas,
    ]) ?>

</div>
