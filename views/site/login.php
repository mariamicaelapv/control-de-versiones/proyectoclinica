<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>




<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
    <body class="is-preload">
        <div id="page-wrapper">
            <header id="header">
                
                <h1><a href="http://localhost/Clinica/web/index.php">Clinica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/index.php">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <!--<h2><img src="web/images/banner.png"></h2><header>-->
                    <h2>Iniciar sesión</h2>
                     <p>Con sus credenciales.</p>
                </header>
            </section>
            <section id="main" class="container medium">
                <div class="box">	

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <br>
                        <a class="btn btn-primary" href="http://localhost/Clinica/web/site/recoverpass">Cambiar Contraseña</a>							
                    
        </div>
<!-- <p><a class="button" href='register'>Registrarse</a></p>
 <p><a class="button" href='recoverpass'>Cambiar Contraseña</a></p>-->
    <?php ActiveForm::end(); ?>

  
    </div>
</div>
