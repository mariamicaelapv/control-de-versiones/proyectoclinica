<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = '';
?>
<html>
    <head>
        <title>Clínica Huellitas - Santander</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />		
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
    <body class="landing is-preload">
        <div id="page-wrapper">
            <header id="header">
                <h1><a href="http://localhost/Clinica/web/index.php">Clínica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a href="http://localhost/Clinica/web/index.php" class="icon solid fa-home">Inicio</a></li>
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <h2>Blog personal</h2>
              <p>Recomendaciones,cuidados,noticias y mucho más...!!</p>					
            </section>
            <section id="main" class="container">					
                <div class="row">
                    <div class="col-12">
                        <section class="box">
                            <div class="row gtr-uniform gtr-50">
                                <div class="col-6 col-12-mobilep">
                                    <h3>Recomendaciónes frente la pandemia </h3>
                                </div>
                                <div class="col-3 col-12-mobilep">
                                    <a class="button special fit icon solid fa-file-contract" href="recomendaciones.pdf" download="Recomendaciones.pdf"> Descargar manual</a>
                                </div>
                              
                                <div class="col-6 col-12-mobilep">
                                    <h4>Lo que debes saber</h4>
                                    <ul>
                                        <li>No conocemos el origen exacto del brote actual de la enfermedad del coronavirus 2019 (COVID-19), pero sabemos que se originó en un animal, probablemente un murciélago.</li>
                                        <li>Por el momento no existe evidencia de que los animales tengan un papel importante en la propagación del SARS-CoV-2, el virus que causa el COVID-19, a las personas.</li>
                                        <li> Con base en la información disponible a la fecha, el riesgo de que los animales transmitan el COVID-19 a las personas se considera bajo.</li>
                                        <li> Se deben realizar más estudios para comprender si diferentes animales podrían resultar afectados por el COVID-19 y de qué manera.</li>
                                        <li>  Las personas con COVID-19 confirmado o presunto deberían evitar tener contacto con animales, incluidas las mascotas, el ganado y la vida silvestre.</li>
                                    </ul>
                                </div>
                                <div class="col-6 col-12-mobilep">
                                    <h4></h4>
                                    <span class="image">  <?= Html::img('@web/css/images/recomendar.jpg')?> </span>
                                
                                </div>	
                            </div>
                        </section>
                        <section class="box">
                            <div class="row gtr-uniform gtr-50">
                                <div class="col-6 col-12-mobilep">
                                    <h3>Baja la fiebre de tu perro </h3>
                                </div>
                                <div class="col-3 col-12-mobilep">
                                   
                               
                                </div>
                                <div class="col-6 col-12-mobilep">
                                    <h4>Lo que debes hacer</h4>
                                    <ul>
                                        <h5>   Existe una alternativa a esto, la medicina casera. Esta opción es viable también para las mascotas y a continuación se presentaran unos tips de qué se debe hacer.</h5>

                                        <li>En caso que la fiebre no sea muy elevada, se puede humedecer una toalla y cubrir al perro con ella. Luego de unos minutos, se debe secar al can.</li>
                                        <li>Es recomendable mantener hidratada a la mascota con agua fría. También brindarle un espacio cómodo para dormir lo ayudará a estar más tranquilo.</li>
                                        <li>Cuando la fiebre es muy alta, hay que bañar al peludo con agua fría durante diez minutos. Es importante no pasarse del tiempo ya que podría ser perjudicial para el perro.</li>
                                        <li>Como última opción, se debe colocar varias bolsas de hielo en las patas traseras y la cabeza por unos cuantos minutos.</li>
                                    </ul>
                                </div>
                                <div class="col-6 col-12-mobilep">
                                    <h4></h4>
                                    <ul>
                                        <span class="image ">  <?= Html::img('@web/css/images/fiebre-perro.jpg')?> </span>
                                    </ul>
                                </div>	
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <footer id="footer">					
                <ul class="copyright">
                    <li>Clínica Huellitas - Proyecto Final de Grado Superior de Desarrollo de Aplicaciones Multiplataforma</li>
                </ul>
            </footer>
        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>