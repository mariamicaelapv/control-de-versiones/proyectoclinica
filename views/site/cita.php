<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
    <body class="is-preload">
        <div id="page-wrapper">
            <header id="header">
                
                <h1><a href="http://localhost/Clinica/web/site/user">Clínica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/user">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
               <h2>Citaciones</h2>
               <p>1-Registre a su mascota.</p>
               <p>2-!Pida cita!</p>
                    
                </header>
            </section>
     
</div>
    </body>
</html>
