<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Clinica Huellitas';
?>
<html>
	<head>
		<title>Clinica Huellitas admin</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">
                                    <h1><a href="http://localhost/Clinica/web/site/admin">Clínica Huellitas</a>- Santander</h1>
					<nav id="nav">
						<ul>
							     <li><a href="logout" class="button">Cerrar Sesión</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2>Gestión de la Clinica Huellitas</h2>
					<p>Administrador</p>
					<ul class="actions special">
						
					</ul>
				</section>

			<!-- Main -->
				<section id="main" class="container">

				

					<div class="row">
						<div class="col-6 col-12-narrower">

							<section class="box special">
                                                             <span class="image featured">   <?= Html::img('@web/css/images/vacunaadmin.jpg')?></span>
								<h3>Vacunas</h3>
								<p>Creación de vacunas</p>
								<ul class="actions special">
									<li><a href="http://localhost/Clinica/web/vacunas" class="button special icon solid fa-search">Crear</a></li>
								</ul>
							</section>

						</div>
						<div class="col-6 col-12-narrower">

							<section class="box special">
                                                           <span class="image featured">   <?= Html::img('@web/css/images/vacunas-animal.png')?></span>
								<h3>Vacunas animal</h3>
								<p>Vacunas puestas a los animales registrados.</p>
								<ul class="actions special">
									<li><a href="http://localhost/Clinica/web/vacunas-animal" class="button special icon solid fa-search">Registar</a></li>
								</ul>
							</section>

						</div>
					</div>

				</section>
			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.facebook.com/profile.php?id=100068521252189" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/clinica_huellitas/" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon brands fa-google-plus"><span class="label">Google+</span></a></li>
                                                               <ul class="copyright">
                    <li>Clinica Huellitas - Proyecto Final de Grado Superior de Desarrollo de Aplicaciones Multiplataforma</li>
					</ul>
				</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>