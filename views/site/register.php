<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\base\model;
use app\models\Users;

?>
<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
    <body class="is-preload">
        <div id="page-wrapper">
            <header id="header">
                
                <h1><a href="http://localhost/Clinica/web/index.php">Clínica Huellitas</a> - Santander</h1>
                <nav id="nav">
                    <ul>
                        <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/index.php">Inicio</a></li>							
                    </ul>
                </nav>
            </header>
            <section id="banner">
                <!--<h2><IMG SRC="web/images/banner.png"></h2><header>-->
                    <h2>Registrate</h2>
                    <p></p>
                </header>
            </section>
            <section id="main" class="container medium">
                <div class="box">					
                   <?php $form = ActiveForm::begin([
    'method' => 'post',
 'id' => 'formulario',
 'enableClientValidation' => true,
 'enableAjaxValidation' => false,
]);
?>


<div class="form-group">
 <?= $form->field($model, "nombre")->input("nombre") ?>   
</div>
<div class="form-group">
 <?= $form->field($model, "apellidos")->input("apellidos") ?>   
</div>
<div class="form-group">
 <?= $form->field($model, "dni")->input("dni") ?>   
        <div class="form-group">
 <?= $form->field($model, "telefono")->input("telefono") ?>   
</div>
</div>
                    <div class="form-group">
 <?= $form->field($model, "username")->input("text") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "email")->input("email") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "password")->input("password") ?>   
</div>

<div class="form-group">
 <?= $form->field($model, "password_repeat")->input("password") ?>   
</div>

<?= Html::submitButton("Register", ["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>
          
                 
             
        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
<h3><?= $msg ?></h3>


