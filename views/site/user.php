<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Clinica Huellitas';

?>

 
<html>
	<head>
		<title>Clinica Huellitas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">
                                    <h1><a href="http://localhost/Clinica/web/site/user">Clínica</a> Huellitas</h1>
					<nav id="nav">
						<ul>
							<li><a href="user">Inicio</a></li>
                                                        <li><a href="contact">Contactar</a></li>
                                                        <li><a href="http://localhost/Clinica/web/mascotas">Registrar mascotas</a></li>
                                                      							<li>
								<!--<a href="site/register" class="icon solid fa-angle-down">Registro</a>-->
							
							</li>
                                                        <li><a href="logout" class="button">Cerrar sesión</a></li>
                                                        </ul>
					</nav>
				</header>


			<!-- Banner -->
				<section id="banner">
					<h2>Clínica Huellitas</h2>
					<p></p>
					<ul class="actions special">
                                          
						<li><a href="http://localhost/Clinica/web/citas" class="button primary">Pedir cita</a></li>
						<li><a href="contact" class="button">Contactar</a></li>
					</ul>
				</section>

			<!-- Main -->
				<section id="main" class="container">

				

					<div class="row">
						<div class="col-6 col-12-narrower">

							<section class="box special">
                                                            <span class="image featured">   <?= Html::img('@web/css/images/productos2.jpg')?></span>
								<h3>Productos</h3>
								<p>Productos disponibles para su compra en tienda.</p>
								<ul class="actions special">
                                                                    
									<li><a href="http://localhost/Clinica/web/productos" class="button special icon solid fa-search">Entrar</a></li>
								</ul>
							</section>

						</div>
						<div class="col-6 col-12-narrower">

							<section class="box special">
							 <span class="image featured"> <?= Html::img('@web/css/images/blog2.jpg')?></span>
								<h3>Blog</h3>
								<p>¡¡Entradas diarias!!</p>
								<ul class="actions special">
									<li><a href="blog" class="button special icon solid fa-search">Leer mas</a></li>
								</ul>
							</section>

						</div>
					</div>

				</section>

			<!-- CTA -->
			

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.facebook.com/profile.php?id=100068521252189" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/clinica_huellitas/" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
						                        <ul class="copyright">
                    <li>Clínica Huellitas - Proyecto Final de Grado Superior de Desarrollo de Aplicaciones Multiplataforma</li>
					</ul>
				</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>