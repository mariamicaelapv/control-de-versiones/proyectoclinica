<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<html>
    <head>
        <title>Clinica huellitas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/main.css" />
        <link  rel="icon"   href="../favicon.png" type="image/png" />
    </head>
    <body class="is-preload">
        <div id="page-wrapper">
            <header id="header">
                

			<!-- Header -->
				
                                    <h1><a href="index">Clinica</a> - Santander</h1>
					<nav id="nav">
						<ul>
                                                     <li><a class="icon solid fa-home" href="http://localhost/Clinica/web/site/user">Inicio</a></li>	
							<li>
								
										
									</li>
								</ul>
							</li>
							
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2>Productos</h2>
					<p>Productos especiales disponibles para su compra en tienda.
                                            Consultas al 942 25 87 45.</p>
					<ul class="actions special">
					
					</ul>
				</section>

			<!-- Main -->
				<section id="main" class="container">

					
<section class="box">
									<h3>Ofertas</h3>

									<h4>Varios</h4>
									<div class="table-wrapper">
										<table>
											<thead>
												<tr>
													<th>Alimento hypoallergenic</th>
													<th>Edad: Todas.
                                                                                                        </th>
													<th>29,90€</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Collar Antiparasitario Scalibor 65 Cm</td>
													<td>El collar antiparasitario Scalibor, de 65 cm de largo, protegerá de parásitos a perros grandes y medianos debido a su poder repelente que aleja a los flebotomos. También protege contra pulgas, garrapatas y mosquitos.</td>
													<td>29.99€</td>
												</tr>
												<tr>
													<td>Hueso Nudo Natural</td>
													<td>Los huesos con nudos sabor natural son un snack excelente para la salud dental. Su masticación no sólo fortalece la mandíbula, si no que también ayuda a limpiar los dientes de manera natural reduciendo el sarro y previniendo la placa.</td>
													<td>1.99€</td>
												</tr>
												<tr>
													<td>Frontline Spray 100 Ml</td>
													<td> RONTLINE SPRAY elimina rápidamente las pulgas, piojos y garrapatas.</td>
													<td>16.14€</td>
												</tr>
												<tr>
													<td>Pestigon Perros 4 Pipetas 0,67 Ml (2-10 Kg)</td>
													<td>Pestigon Pipetas para perros es el nuevo antiparasitario spot-on de Laboratorios Karizoo para el tratamiento de infestaciones por pulgas y garrapatas (también puede ser usado como parte del tratamiento de la DAPP (Dermatitis Alérgica a la picadura de las pulgas).</td>
													<td>11.48€</td>
												</tr>
												<tr>
													<td>Seresto Perros +8 Kg</td>
													<td>El collar Seresto más de 8 kg no es como la mayoría de los collares contra pulgas y garrapatas. Se ha desarrollado utilizando una novedosa combinación de materiales que posibilitan la liberación de los principios activos de una forma controlada, a dosis muy bajas, y ofrece a tu perro hasta 8 meses de protección continuada contra pulgas y garrapatas.</td>
													<td>27.95€</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="2"></td>
													
												</tr>
											</tfoot>
										</table>
									</div>

									
					<div class="row">
                                            			<h3>Comida para gatos</h3>

									
									<div class="table-wrapper">
										<table>
											<thead>
												<tr>
													<th>Alimento Adult Cat Pescado Blanco</th>
													<th>Alimento completo para gatos adultos preparado con pescado blanco fresco, que lleva lo mejor del mar a tu mascota</th>
													<th>14,95€</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Orijen Fit & Trim Cat</td>
													<td>Pienso para gatos que deben perder peso y que necesitan una dieta baja en calorías. El alimento Orijen está elaborado con carne y pescado fresco y le aportará todos los nutrientes que necesita para estar sano y fuerte. </td>
													<td>24.99€</td>
												</tr>
												<tr>
													<td>Royal Canin Gato Kitten.</td>
													<td> Este pienso contiene un complejo de antioxidantes para que tu gatito tenga unas defensas más fuertes. Además, su fórmula posee una mezcla equilibrada de vitaminas y minerales adaptadas a las necesidades nutricionales del gato.</td>
													<td>16.99€</td>
												</tr>
												<tr>
													<td>FA2 - Cat Allergy Hipoallergenic 3 Kg</td>
													<td>SI tu gato presenta sensibilidad a ciertos alimentos o sufre reacciones cutáneas, puede que resulte útil recurrir a un pienso hipoalergénico con una cantidad reducida de ingredientes que te ayude a descubrir cuáles son los que no tolera tu felino. Virbac Vet HPM Cat Allergy A2 es ideal como alimento durante una dieta de exclusión o como comida completa a largo plazo.</td>
													<td>42.14€</td>
												</tr>
												<tr>
													<td>Virbac HPM Baby Pre Neutered Cat 1,5 Kg</td>
													<td>Dieta completa para gatitos y gatas en gestación / lactación.</td>
													<td>21.48€</td>
												</tr>
												<tr>
													<td>Specific Allergen Management Plus 100 Gr</td>
													<td>SPECIFIC ALLERGEN MANAGEMENT PLUS 100 g FOW-HY</td>
													<td>1.95€</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="2"></td>
													
												</tr>
											</tfoot>
										</table>
									</div>
						<div class="col-12">
					</section>

				

							
						</div>
					</div>

				</section>

		
				

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon brands fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>