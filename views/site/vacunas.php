
<?php
use dosamigos\datetimepicker\DateTimePicker;
?>
<?= DateTimePicker::widget([
    'model' => $model,
    'attribute' => 'end_time',
    'language' => 'zh-CN',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd hh:ii',
        'todayBtn' => true,
        'pickerPosition' => "bottom-left",
    ]
]);?>
<script>
    $(function () {
        $('.glyphicon-remove').parent().remove();
        $('.date').css('width', '220px');
        var beginTime = '<?=date('Y-m-d H:i', $model->begin_time);?>';
        var endTime = '<?=date('Y-m-d H:i', $model->end_time);?>';
        if(beginTime == '1970-01-01 08:00' || endTime == '1970-01-01 08:00') {
            beginTime = timestampToTime(Date.parse(new Date()) / 1000);
            endTime = timestampToTime(Date.parse(new Date()) / 1000);
        }
        $('#outage-begin_time').val(beginTime);
        $('#outage-end_time').val(endTime);
    });
 
    /**
     * Tiempo de giro de marca de tiempo
           * @param marca de tiempo
           * @returns {string} time
     */
    function timestampToTime(timestamp) {
        if(timestamp == 0) {
            return '-';
        }
                 var date = new Date (timestamp * 1000); // Se requiere 1000 timestamp para * 1000, no se requiere 13 timestamp para multiplicar por 1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = date.getDate() + ' ';
        var h = date.getHours() + ':';
        var m = date.getMinutes();
        return Y + M + D + h + m ;
    }
