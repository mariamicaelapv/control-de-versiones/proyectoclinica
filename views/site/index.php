<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Clínica Huellitas';
?>
<html>
	<head>
		<title>Clinica Huellitas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">
                                    <h1><a href="index.php">Clínica</a> Huellitas</h1>
					<nav id="nav">
						<ul>
							<li><a href="index.php">Inicio</a></li>
                                                        <li><a href="site/contact">Contactar</a></li>
							<li>
							<a href="site/register">Registrarse</a>
							</li>
                                                        <li><a href="site/login" class="button">Iniciar Sesión</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2>Clínica Huellitas</h2>
					<p></p>
					<ul class="actions special">
						<li><a href="site/login" class="button primary">Iniciar Sesión</a></li>
						<li><a href="site/contact" class="button">Contactar</a></li>
					</ul>
				</section>

			<!-- Main -->
				<section id="main" class="container">

				

					<div class="row">
						<div class="col-6 col-12-narrower">

							<section class="box special">
                                                            <span class="image featured"><img src="../web/css/images/perro-gato.png" alt="" /></span>
								<h3>Productos</h3>
								<p>Productos disponibles para su compra en tienda.</p>
								<ul class="actions special">
									<li><a href="productos" class="button special icon solid fa-search">Entrar</a></li>
								</ul>
							</section>

						</div>
						<div class="col-6 col-12-narrower">

							<section class="box special">
							 <span class="image featured"><img src="../web/css/images/perro-cama.jpg" alt="" /></span>
								<h3>Blog</h3>
								<p>¡¡Entradas diarias!!</p>
								<ul class="actions special">
									<li><a href="site/blog" class="button special icon solid fa-search">Leer mas</a></li>
								</ul>
							</section>

						</div>
					</div>

				</section>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.facebook.com/profile.php?id=100068521252189" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/clinica_huellitas/" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon brands fa-google-plus"><span class="label">Google+</span></a></li>
                                                               <ul class="copyright">
                    <li>Clinica Huellitas - Proyecto Final de Grado Superior de Desarrollo de Aplicaciones Multiplataforma</li>
					</ul>
				</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>