<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Referencia */

$this->title = 'Update Referencia: ' . $model->ref_animal;
$this->params['breadcrumbs'][] = ['label' => 'Referencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ref_animal, 'url' => ['view', 'id' => $model->ref_animal]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="referencia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
