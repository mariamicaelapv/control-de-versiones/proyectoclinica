<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Referencia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="referencia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ref_vacuna_animal')->textInput() ?>

    <?= $form->field($model, 'ref_animal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
