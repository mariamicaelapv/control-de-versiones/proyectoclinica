<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */

$this->title = 'Update Telefonos: ' . $model->ref_vet;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ref_vet, 'url' => ['view', 'id' => $model->ref_vet]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
